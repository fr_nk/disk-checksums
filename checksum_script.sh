#!/bin/bash


SELF=$(basename $0)
VERSION=0.3
YEAR=2017
URL='https://gitlab.com/fr_nk/disk-checksums'
SHASUM=`which shasum`
FIND=`which find`
GREP=`which grep`
DATE=`which date`

STRENGTH=512

usage="USAGE:

  $SELF [-v] [-p PATH-NAME] [VOLUME-NAME] [CHK-FILE]
    script to generate checksums for all files on a volume. The output will be
    saved in the current path in a .chk.txt file. The file name depends on the
    current date, followed by the name of the volume or path for which the
    checksum are generated. The script has to be run through a shell, eg by
    calling 'bash $SELF' or by making it executable and calling
    './$SELF'. The following description uses the first approach.


  VOLUME-NAME
    If the specified argument is not a .chk.txt file, this script assumes it is
    the name of the hard disc or volume to be checked. It will use the path
    '/Volumes/VOLUME-NAME' as a base. The following example generates checksums
    for all files on the volume '/Volumes/FI-214-IT'.

    ‘bash checksum_script.sh FI-214-IT'


  -p PATH-NAME

    If the files to be checked are not on a volume under the /Volumes/ folder,
    then you might want to use this option to check another path. This option
    only makes sense in the case of checksum generation and can be used instead
    of VOLUME-NAME. Please specify the full path name. For example you would
    like to keep a checksum for the files on your Desktop, then you could run
    the following example:

    'bash checksum_script.sh /Users/FrankLoesche/Desktop/'


  CHK-FILE

    If the specified argument is a chk.txt file, then this script will attempt
    to check the checksums from this file. Make sure that the hard disc or
    volume is properly connected to the computer, otherwise this check will
    fail. If you previously created a '2017-04-28_FI-214-IT.chk.txt' file, then
    the following example would check the volume '/Volumes/FI-214-IT' for any
    changes since the checksums were created. The output will contain a line
    with the file count, the failed and the successful checks and will look
    similar to: 'Files checked: 2; Failed: 0; OK: 2'. Failed should always be 0,
    otherwise some file have been changed.

    'bash checksum_script.sh 2017-04-28_FI-214-IT.chk.txt'


  -v
    print the version of this script, all applications involved, and a link to
    the author. If you have any problems with the script, please copy and paste
    the full information into an email and send it to the author:

    'bash checksum_script.sh -v'

$SELF: v$VERSION $YEAR; $URL"

help="$SELF: v$VERSION $YEAR
$SHASUM: v`$SHASUM --version`
$GREP: `$GREP --version | head -n 1`

If you have questions about the script please contact the author Frank Loesche <loesche@tamotua.de> or Guy Edmonds <guymondo@gmail.com> and send along this output. The most current version of this script is always at $URL.
"

ispath=FALSE
docheck=FALSE


while getopts ':pvhs:' option; do
  case "$option" in
    v) printf %"s\n" "${help}"
       exit
       ;;
    h) echo "$usage"
       exit
       ;;
    p) ispath=TRUE
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage"
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage"
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

printf "started at %s\n" "`date`"

if [ -z ${1+x} ]; then
  printf %"s\n" "$SELF ERROR: No volume or path is specified\n\n" >&2
  printf %"s\n" "${usage}"
  exit 1
fi


# If argument 1 is a file, try to interpret it as a .chk.txt file
if [ -f "${1}" ]; then
  printf %"s\n" "Attempting to check files"
  docheck=TRUE
fi


# Compare checksums from .chk.txt file with real files and exit
if [ ${docheck} == TRUE ]; then
  # this line does the real work:
  # shasum -c FILENAME.chk.txt
  CHECKED=$(${SHASUM} -c ${1} | tee .tmpfile | wc -l)
  FAILED=$(cat .tmpfile | grep -v OK | wc -l)
  FINE=$(cat .tmpfile | grep -o OK | wc -l)
  echo "Files checked: $CHECKED; Failed: $FAILED; OK: $FINE"
  if [[ FAILED -gt 0 ]]; then
    cat .tmpfile | grep -v OK > FAILED_FILES.txt
  fi
  rm -f .tmpfile
  #head -n1 2017-02-21_base.chk.txt | while read a b; do echo "$b"; done
  printf "successfully finished at %s\n" "`date`"
  exit
fi


# Generate new .chk.txt file
if [ ${ispath} == TRUE ]; then
  CHKPATH=${1}
else
  CHKPATH="/Volumes/${1}"
fi

if [ ! -d "${CHKPATH}" ]; then
  printf %"s\n" "${SELF} ERROR: Path ${CHKPATH} does not exist. Please double check that it was entered correctly." >&2
  exit 2
fi

SHORT=$(basename ${CHKPATH})

# This line does the real work. If you just want to run it by itself, use something like:
# find "YOURPATH" -type f -exec shasum -a 512 {} \; > sums.chk.txt
${FIND} "${CHKPATH}" -type f -exec ${SHASUM} -a ${STRENGTH} {} \; > `${DATE} +%Y-%m-%d`_${SHORT}.chk.txt
printf "successfully finished at %s\n" "`date`"
